# Copyright (c) 2022 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

UNAME:=$(shell id -un)
GNAME:=$(shell id -gn)
UID:=$(shell id -u)
GID:=$(shell id -g) 

# If the first argument is ...
ifneq (,$(findstring tools_,$(firstword $(MAKECMDGOALS))))
	# use the rest as arguments
	RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
	# ...and turn them into do-nothing targets
	#$(eval $(RUN_ARGS):;@:)
endif

.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[\.0-9a-zA-Z_-]+:.*?## / {printf "\033[36m%-42s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

build-cpu: ## Build Whisper [CPU] Container
	docker build --target whisper-cpu -t okdhryk/whisper:cpu -f docker/Dockerfile .
	@printf "\n\033[92mBuild Docker Image: okdhryk/whisper:cpu\033[0m\n"

build-gpu: ## Build Whisper [GPU] Container
	docker build --target whisper-gpu -t okdhryk/whisper:gpu -f docker/Dockerfile .
	@printf "\n\033[92mBuild Docker Image: okdhryk/whisper:gpu\033[0m\n"


restart: stop run

run-cpu: ## RUN Whisper [CPU]  Container
	docker run --rm -it --privileged --net=host --ipc=host \
		--device=/dev/dri:/dev/dri --device=/dev/snd:/dev/snd \
		-v ~/.cache/whisper:/root/.cache/whisper/ \
		-v $(PWD):/workspace/  \
		okdhryk/whisper:cpu
	@printf "\n\033[92mRun Docker Image: okdhryk/whisper:cpu\033[0m\n"

run-gpu: ## RUN Whisper [GPU]  Container
	docker run --rm -it --privileged --net=host --ipc=host \
		--gpus all \
		--env PULSE_SERVER=unix:/tmp/pulseaudio.socket \
    		--env PULSE_COOKIE=/tmp/pulseaudio.cookie \
    		--volume /tmp/pulseaudio.socket:/tmp/pulseaudio.socket \
    		--volume /tmp/pulseaudio.client.conf:/etc/pulse/client.conf \
		--device=/dev/dri:/dev/dri --device=/dev/snd:/dev/snd \
		-v ~/.cache/whisper:/root/.cache/whisper/ \
		-v $(PWD):/workspace/  \
    		--user $(UID):$(GID) \
		okdhryk/whisper:gpu 
	@printf "\n\033[92mRun Docker Image: okdhryk/whisper:gpu\033[0m\n"
	

up: ## docker-compose up Noetic [CPU] Base Container
	docker-compose up

contener=`docker ps -a -q`
image=`docker images | awk '/^<none>/ { print $$3 }'`

stop: ## STOP All Container
	docker rm -f $(contener)

attach: ## ATTACH Noetic [CPU] Base Container
	docker exec -it $(NAME) /bin/bash

logs: ## Logs Noetic [CPU] Base Container
	docker logs $(NAME)


