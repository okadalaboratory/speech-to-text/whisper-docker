# WhishperをDockerで動かす
OpenAIから発表されたオープンソースの音声認識モデル「Whisper」は68万時間もの訓練データで訓練された大規模な音声認識モデルです。

日本語を含む多言語の音声認識、音声翻訳、言語認識、音声区間検出なども可能です。

ここではDockerを使い、ROSから簡単にWhisperを使ってみます。

<img src="images/Whisper1.png" width="640">

**マイクが接続されているPCで実行する必要がある。**

# To Do
- 19 Feb. 2023<br>
認識パラメータを変更できるようにする

- 19 Feb. 2023<br>
~~ダウンロードしたモデルデータを再利用して高速化する<br>~~
Docker　runの際に -v ~/.cache/whisper:/root/.cache/whisper/ 

- 18 Feb. 2023<br>
ROSで使えるようにする<br>
/whisper/result に結果を配信（OpenHRIと同じ型）

- 14 Feb. 2023<br>
~~Dockerでマイク入力や音声出力ができない~~
Docker　runの際に --device=/dev/snd:/dev/snd <br>
ros_entrypoint.sh で　 useradd -m $USER -G sudo,video,audio<br>
ユーザをaudioグループに加える

# Whishperとは
[公式サイト](https://openai.com/blog/whisper)<BR>
[論文](https://cdn.openai.com/papers/whisper.pdf)<br>
[Github](https://github.com/openai/whisper)<br>

## Whisperのモデル
多言語：tiny, base, small, medium, large<br>
英語：tiny.en, base.en, small.en, medium.en, large<br>
```
whisper_model = whisper.load_model('small', download_root='./')
```
使用しているGPUボードの仕様により、mediumやlargeはリソース不足になる場合がある

## Python　API
https://dev.classmethod.jp/articles/whisper-how-to/

- transcribe(書き起こし処理)
音声からの文字書き起こし。
99言語に対応
- translate(書き起こし + 翻訳)
音声からの翻訳処理。
入力は多言語に対応していますが、出力は英語のみ。
- 言語判定や有音無音判定（VAD：Voice Activation Detector）

## transcribe関数
```
result = model.transcribe(
    "test.wav",
    
    )
print(result["text"])

```
### 出力
json形式
```
$ result.kesy()
dict_keys(['text', 'segments', 'language'])
```
- ‘text’: 文字起こしをしたテキストデータ
- ‘segments’:文節ごとのセグメントデータのリスト
- ‘language’:認識した言語

```
$ result['test']
'今日はいい天気ですね'

$ result['segments'][0]
{'id': 0,
 'seek': 0,
 'start': 0.0,
 'end': 2.7600000000000002,
 'text': '今日はいい天気ですね',
 'tokens': [20083, 2972, 15940, 8945, 3065, 18216, 26983, 4801, 27113],
 'temperature': 0.0,
 'avg_logprob': -0.2711757566870713,
 'compression_ratio': 1.168141592920354,
 'no_speech_prob': 0.041641715914011}

$ result['language']
'ja'
```

### パラメータ
- audio<br>
音声ファイルへのパス

- verbose<br>
**True**に設定すると認識中に結果を出力する（デフォルトは**False**)

- logprob_threshold<br>
ログを返した割合の閾値。デフォルトは**-1.0**。この値を”avg_logprob”が下回ると、失敗と判断する。

- no_speech_threshold<br>
無音と判断する閾値。デフォルトは**0.6**<br>
”no_speech_prob”がこの値を上回り、”avg_logprob”が”logprob_threshold”を下回った場合、「無音」と判断する。

- condition_on_previous_text<br>
前の出力を参照して、一貫性のある文章を出力してくれる。デフォルトは**True**。**False**にすると、前後の文脈の一貫性がなくなるが、まれに発生する繰り返しループにハマることを避けることができる。

**decode_options**<br>
- task<br>
    - “transcribe”（デフォルト）：文字起こし
    - “translate”：英語に翻訳
```
ja_result = model.transcribe("test.wav")
en_result = model.transcribe("test.wav", task="translate")
```

- language<br>
自動識別するが、あらかじめ分かっている場合は指定できる
```
language="ja"
language="en"

```
- sample_len<br>
サンプリングするトークンの数の最大値を設定。

- best_of<br>
ビームサーチで収集する独立したサンプル数

- beam_size<br>
ビームサーチで設定するビーム数

- patience<br>
ビームサーチにおけるpatienceで設定されるパラメータ。生成に対し、さらに候補をあげて改善しようとするか決定するロジックに関係する

- prompt<br>
その文章の前に置かれるテキストやトークンのリスト

- prefix<br>
その文章の後ろに置かれるテキストやトークンのリスト

- suppress_blank<br>
無用な空白の生成を抑える。デフォルトでTrue

- suppress_tokens<br>
リストでトークンのIDを与え、そのトークンを結果から除外する

- without_timestamps<br>
Trueにするとタイムスタンプを使わず、textトークンのみを出力します。（segmentsで分かれなくなる）

- max_initial_timestamp<br>
最初のタイムスタンプを出力する最大の時間を設定



## 設定
```
--temperature TEMPERATURE
                       temperature to use for sampling (default: 0)
 --best_of BEST_OF     number of candidates when sampling with non-zero temperature (default: 5)
 --beam_size BEAM_SIZE
                       number of beams in beam search, only applicable when temperature is zero (default: 5)
 --patience PATIENCE   optional patience value to use in beam decoding, as in https://arxiv.org/abs/2204.05424, the default (1.0) is equivalent to conventional
                       beam search (default: None)
 --length_penalty LENGTH_PENALTY
                       optional token length penalty coefficient (alpha) as in https://arxiv.org/abs/1609.08144, uses simple length normalization by default
                       (default: None)
 --suppress_tokens SUPPRESS_TOKENS
                       comma-separated list of token ids to suppress during sampling; '-1' will suppress most special characters except common punctuations
                       (default: -1)
 --initial_prompt INITIAL_PROMPT
                       optional text to provide as a prompt for the first window. (default: None)
 --condition_on_previous_text CONDITION_ON_PREVIOUS_TEXT
                       if True, provide the previous output of the model as a prompt for the next window; disabling may make the text inconsistent across
                       windows, but the model becomes less prone to getting stuck in a failure loop (default: True)
 --fp16 FP16           whether to perform inference in fp16; True by default (default: True)
 --temperature_increment_on_fallback TEMPERATURE_INCREMENT_ON_FALLBACK
                       temperature to increase when falling back when the decoding fails to meet either of the thresholds below (default: 0.2)
 --compression_ratio_threshold COMPRESSION_RATIO_THRESHOLD
                       if the gzip compression ratio is higher than this value, treat the decoding as failed (default: 2.4)
 --logprob_threshold LOGPROB_THRESHOLD
                       if the average log probability is lower than this value, treat the decoding as failed (default: -1.0)
 --no_speech_threshold NO_SPEECH_THRESHOLD
                       if the probability of the <|nospeech|> token is higher than this value AND the decoding has failed due to `logprob_threshold`, consider
                       the segment as silence (default: 0.6)
 --threads THREADS     number of threads used by torch for CPU inference; supercedes MKL_NUM_THREADS/OMP_NUM_THREADS (default: 0)
```
# 先ずは体験してみる
ブラウザーから<br>
[huggingfaceさんのデモ](https://huggingface.co/spaces/openai/whisper)<br>
かなり時間が必要なので気長に待ってください。

Dockerを使って<br>
[Dockerを使ってOpenAIのWhisperをサクッと試す](https://zenn.dev/kento1109/articles/d7d8f512802935)

# 先人の知恵
## Google Colaboratoryで試す
[WhisperとGoogle Colaboratoryで音声の文字起こしをやってみた](https://zenn.dev/tam_tam/articles/d59250ecf25628)<br>
[OpenAIの音声認識Whisperがすごいので，Google Colabで試してみた](https://qiita.com/walnut-pro/items/0124a5a0c83c9b4e2669)<br>
[Google Colab で はじめる OpenAI Whisper](https://note.com/npaka/n/neb755633eb43)<br>
[【Whisper】Pythonで音声ファイルを書き出ししてみよう！](https://aiacademy.jp/media/?p=3512)<br>
[【AI Shift Advent Calendar 2022】Whisperによる音声認識のTips](https://www.ai-shift.co.jp/techblog/3093)

## Dockerで使う
### 音声ファイルを読み込ませる
[Dockerを使ってOpenAIのWhisperをサクッと試す](https://zenn.dev/kento1109/articles/d7d8f512802935)<br>

[whisper_demo](https://github.com/Gyabi/whisper_demo)

### マイク入力（一度、音声ファイルに保存するので少し無駄ですけど）
[マイク入力をWhisperで音声認識](https://github.com/karaage0703/whisper-docker)<br>
## エラー
mic.pyを実行の際に下記のようなエラーが出た場合
```
$ python3 mic.py
arecord: main:788: audio open error: デバイスもしくはリソースがビジー状態です
```
```
＄pkill -9 arecord
```

デバイスを確認し、mic.pyを修正する
```
cmd = 'exec arecord -D plughw:0,0 -r 16000 -f S16_LE ./input.wav'

cmd = 'exec arecord -D plughw:1,0 -r 16000 -f S16_LE ./input.wav'
```

### マイク入力(ストリーミング入力)
[声をPythonに聴かせて（マイクから入力した声をWhisperに、何度でも認識させよう）](https://nikkie-ftnext.hatenablog.com/entry/my-first-shion-speech-recognition-whisper-microphone-repeatedly)


# 環境
- Ubuntu20.04
- Docker
- NVIDA

# Dockerイメージの作成
```
$ cd ~
$ git clone https://gitlab.com/okadalaboratory/myprojects/whisper-docker.git
$ cd whisper-docker
$ make build-gpu
$ make run-gpu
```

## Makefile
Dockerイメージの作成やコンテナの実行はmakeコマンドで行います。

下記の通り　make help コマンドで確認することができます。
```

```
# 工事中
```
roslaunch 
```

# 工事中


# これから
## 高速化
[音声認識モデル Whisper の推論をほぼ倍速に高速化した話](https://qiita.com/halhorn/items/d2672eee452ba5eb6241)<br>
- 2022年末現在の音声認識最強モデル Whisper を高速化
- 重みの fp16 化
- TorchScript 化
- 認識の長さを30秒ごとから10秒ごとに
- 結果処理速度が約2倍に

[音声認識モデル”Whisper”をストリーミング処理対応させる方法](https://dev.classmethod.jp/articles/whisper-streaming/)

## 追加学習
[OpenAI Whisper に追加学習をさせる試み](https://eng-blog.iij.ad.jp/archives/15499)


# 参考
[Introducing Whisper - OpenAI](https://openai.com/blog/whisper/)
https://github.com/openai/whisper

[OpenAIリリースの音声認識モデル「Whisper」APIの無償提供を開始いたします](https://prtimes.jp/main/html/rd/p/000000007.000063429.html)

[OpenAIがリリースした音声認識モデル”Whisper”の使い方をまとめてみた](/https://dev.classmethod.jp/articles/whisper-how-to)

[OpenAIの文字起こしAI「Whisper」の使い方](https://aismiley.co.jp/ai_news/what-is-whisper/)

[出村さん ROS化](　https://demura.net/robot/hard/20363.html)

[マイク入力をWhisperで音声認識](https://zenn.dev/karaage0703/articles/d47bbb085fcb83)


# 作者
岡田浩之<br>
メール: hiroyuki.okada@okadanet.org

# ライセンス
[MIT](http://opensource.org/licenses/mit-license.php)</blockquote>
