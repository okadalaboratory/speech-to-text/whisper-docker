#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

# Whisperによる音声認識
# whisper_recognizer
# 結果出力トピック /whisper/result
from io import BytesIO
import numpy as np
import soundfile as sf
import speech_recognition as sr
import whisper

from lxml import *
from bs4 import BeautifulSoup
from xml.dom.minidom import Document

import rospy
from std_msgs.msg import String

def whisper_recognizer(model="small"):
    # ノードを初期化する。
    rospy.init_node("whisper_recognizer", anonymous=True) 
    rospy.loginfo("whisper start node : whisper_recognizer")

    # 送信者を作成する。
    result_pub = rospy.Publisher("/whisper/result", String, queue_size=1) 
    rospy.loginfo("whisper start publisher : /whisper/result")

    # Whisper認識モデルのダウンロード
    # ~/.cache/whisperに保存され再利用される
    model = whisper.load_model(model)
    rospy.loginfo("whisper model download : %s",model)

    try:
        print("A moment of silence, please...")
        with sr.Microphone(sample_rate=16_000) as source: sr.Recognizer().adjust_for_ambient_noise(source)
        print("Set minimum energy threshold to {}".format(sr.Recognizer().energy_threshold))

        rate = rospy.Rate(10)
        while not rospy.is_shutdown():  
            print("Say something!")
            with sr.Microphone(sample_rate=16_000) as source: audio = sr.Recognizer().listen(source)
            print("Got it! Now to recognize it...")
            try:
                wav_bytes = audio.get_wav_data()
                wav_stream = BytesIO(wav_bytes)
                audio_array, sampling_rate = sf.read(wav_stream)
                audio_fp32 = audio_array.astype(np.float32)

                result = model.transcribe(audio_fp32, fp16=False)
                print(result["text"])
                result_pub.publish(result["text"])
            except:
                pass
            rate.sleep() 

    except KeyboardInterrupt:
        pass   

if __name__ == '__main__':
    try:
        whisper_recognizer("small")
    except rospy.ROSInterruptException: pass
