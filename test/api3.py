import whisper
import torch

checkpoint = torch.load('/root/.cache/whisper/small.pt', map_location='cpu')
dims = whisper.model.ModelDimensions(**checkpoint["dims"])
dims.n_audio_ctx = 500  # 10s

model = whisper.model.Whisper(dims)
for k, p in model.state_dict().items():
    p.copy_(checkpoint["model_state_dict"][k])

model.encoder = torch.jit.script(model.encoder)
model.decoder = torch.jit.script(model.decoder)
_ = model.half()
_ = model.cuda()

result = model.transcribe(
        "jfk.flac",
        verbose=True,
        language='japanese',
        beam_size=5,
        fp16=True,
        without_timestamps=True
    )
print(result["text"])
