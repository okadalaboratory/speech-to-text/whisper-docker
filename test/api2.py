import whisper
import torch

model = whisper.load_model("small",device="cpu")
model.encoder = torch.jit.script(model.encoder)
model.decoder = torch.jit.script(model.decoder)
_ = model.half()
_ = model.cuda()

result = model.transcribe(
        "input.wav",
        verbose=True,
        language='japanese',
        beam_size=5,
        fp16=True,
        without_timestamps=True
    )
print(result["text"])
