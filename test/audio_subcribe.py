#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

# openhri_ros において認識結果を解析する
# トピック /
# <include file="$(find audio_capture)/launch/capture_wave.launch">
#  <arg name="ns" value="audio_capture"/>
# </include>
#
# /audio_capture/audio
# /audio_capture/audio_info
# /audio_capture/audio_stamped
import rospy
from std_msgs.msg import String
from audio_common_msgs.msg import AudioData

def callback(data):
    rospy.loginfo(rospy.get_caller_id()+"I heard %s",data.data)
    
def listener():

    # in ROS, nodes are unique named. If two nodes with the same
    # node are launched, the previous one is kicked off. The 
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaenously.
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("/audio_capture/audio",AudioData
, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
        
if __name__ == '__main__':
    listener()
