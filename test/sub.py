#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

# openhri_ros において認識結果を解析する
# トピック /julius/result
import xml.etree.ElementTree as ET 
import rospy
from std_msgs.msg import String
# 認識を受け入れる閾値
THRESHOLD = 0.5

def callback(msg):
    # Juliusの認識結果が /julius/result　に配信される
    rospy.loginfo("Recieved:'{}'".format(msg.data))

    # 認識結果の辞書型配列を初期化する
#    result_dic={}

#    root=ET.fromstring(msg.data)
    # 全ての子要素（認識候補）を取得し、辞書型配列に追加する
#    for child in root:
#        result_dic[child.attrib['text']]= float(child.attrib['score'])

    # 認識候補を　score　の大きい順に並び替える
#    result_sorted = sorted(result_dic.items(), key = lambda score : score[1], reverse=True)

#    if len(result_sorted) == 0:
#        rospy.loginfo("No candidates for recognition were found")
    
#    if result_sorted[0][1] < THRESHOLD:
#        rospy.loginfo("No candidates exceeded the threshold")
#    else:
#        rospy.loginfo("Text:%s, Score:%f",result_sorted[0][0],result_sorted[0][1])

def subscriber():
    # ノードを初期化する。
    rospy.init_node("subscriber")
    # 受信者を作成する。
    rospy.Subscriber("/whisper/result", String, callback)
    # ノードが終了するまで待機する。
    rospy.spin()

if __name__ == "__main__":
    subscriber()

